Changelog - By Lorenzo Franceschini

09/03/2018

* Cambiato tutto :D w il venerdì sera. Utilizzati @Decoratori per @Controller e @Middleware grazie alla lib @decorators/express;
* Rotto il test dell'About in quanto devo rifare il controller.

08/03/2018

* Stravolto il sistema dei modelli per MongoDB, con approccio a classi, classi base e interfacce;
* Incomincio a delineare le classi per la tabella Profiles di PostgreSQL;
* Cambiato il test per la tabella di PostgreSQL ed inserito il beforeEach;
* Aggiunto un README.md ;D

07/03/2018

* Create le credenziali di accesso con (https://console.developers.google.com/apis/credentials)[Link] e inserite nel config;
* Installata la dipendenza per la strategia di Passport per Google OAuth 2.0
npm install -S passport-google-oauth2
npm install -D @types/passport-google-oauth2
* Create due routes nel controller Account relative all'accesso con Google;
* Eliminato la dipendenza per passport-local e passport-local-mongoose e relative @types;
* Aggiunto un primo step per PostgreSQL;

06/03/2018

* Ho aggiunto il middleware di validazione dei campi request per migliorare la leggibilità;
* Il controller deve essere una funzione che prende una ROUTE e chiama i vari middleware/services;
* Probabilmente i services verranno spostati nella sezione middleware se lavorano direttamente sugli oggetti Req e Res per ovvi motivi di definizione del middleware stesso, mentre un services potrà essere una funzione che accede a qualcosa di esterno;
* Spostate alcune logiche dal modello Accont alla classe Auth (middleware/servizio);
* A questo punto la passport-local strategy diventa inutile. Si toglierà successivamente.

Tests:

Ricorda di aggiornare il JWT di Autorizzazione per la richiesta della pagina protetta ;)