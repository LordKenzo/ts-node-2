# Mongoose

_I Documents sono istanze dei nostri Model! (doc mongoose)_

Un **modello** (che rappresenta la collezione) di mongoose implementa dei metodi di "lettura" che possiamo definire di default per tutti i modelli della collezione e sono:
  * findById (potenzialmente null)
  * findOne (potenzialmente null)
  * find (lista di documenti anche vuota)
a questi potremmi aggiungere un metodo conveniente:
  * all (lista di tutti di documenti anche vuota)

Per i metodi di "scrittura" invece troviamo:
  * create
  * update
  * remove

Per questo motivo possiamo creare due interfacce di IRead e IWrite che dichiarano questi metodi e creare una classe base di Modello per i modelli concreti.

Le interfacce saranno di tipo <T> in quanto dovranno accettare diversi modelli.


# Social Token

Vedi:
https://codeburst.io/node-js-rest-api-facebook-login-121114ee04d8

# Riferimenti

[https://gallery.technet.microsoft.com/Application-Example-NodeJS-d632ee2d](Technet Microsoft)
[http://mongoosejs.com/docs/api.html](API Mongoose)
[http://docs.sequelizejs.com/manual/tutorial/models-usage.html](Sequelize)
[https://facebook.github.io/jest/](JEST)
[https://github.com/serhiisol/node-decorators/tree/master/express](Libreria Express @Decorators)