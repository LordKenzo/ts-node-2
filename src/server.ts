import express, { Application, Request, Response } from 'express';
import routes from './routes';
import passport from 'passport';
import bodyParser from 'body-parser';
import morgan from 'morgan';
import {initPassport} from './config/passport';
import { apiError } from './controller/errors';


export default function server() {

  const app:Application = express();

  app.use(morgan('dev'));
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({extended: true}));
  initPassport(app);

  app.get('/', (req: Request, res: Response) => {
    res.status(200).send({ 'Page': 'Home Page' });
    res.end();
  });
  app.use('/v1', routes)
  app.use(apiError);
  app.listen(8080, () => {
    console.log('Server is Up!');
  });
}
