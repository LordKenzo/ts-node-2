import { Router, Response as Res, Request as Req, NextFunction } from "express";
import passport from 'passport';
import jwt from 'jsonwebtoken';
import {config} from '../config'
import { Post, Get, Request, Response, Next, Middleware, Controller } from "@decorators/express";

export class VerifyToken implements Middleware{

  public use(req: Req, res: Res,next: NextFunction): void {
    if(typeof req.headers.authorization === 'string'){
      jwt.verify(req.headers.authorization, config.server.SECRET_JWT, function(err, decoded: any) {
        if(err){
          next(err);
        } else {
          req.user = {
            id: decoded["id"] || undefined,
            email: decoded["email"] || undefined
          }
        }
      });
    } else {
      next('Nessun Token inviato');
    }

    next();
  }

}

export class GenerateToken implements Middleware{

  public use(req: Req, res: Res,next: NextFunction): void {
    console.log('Generate Token', req.user);
    req.user = req.user || {};
    req.headers.authorization = jwt.sign({
      id: req.user.id,
      email: req.user.email
    }, config.server.SECRET_JWT, {
      expiresIn: config.server.EXPIRES_JWT
    });
    next();
  }

}

export class SendToken implements Middleware{

  public use(req: Req, res: Res,next: NextFunction): void {
    req.user = req.user || {};
    console.log('Respond Token', req.user);
    res.status(200).json({
      SCOPO_DEBUG_ELIMINAMI: req.user, // DA ELIMINARE :)
      token: req.headers.authorization
    });
  }

}
