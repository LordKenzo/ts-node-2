import { Request, Response, NextFunction } from "express";
import { Middleware } from "@decorators/express";

export class checkEmailPassword implements Middleware{
  public use(request: Request, response: Response, next: NextFunction): void {
    console.log('param', request.body.password, request.body.email);
    if(!request.body.password)
      next({errore: "Manca la password"})
    else if(!request.body.email){
      next({errore: "Manca l'email"})
    } else {
      next();
    }

  }
}