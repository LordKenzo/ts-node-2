import { Request as Req, Response as Res, NextFunction } from "express";
import { Controller, Post, Get, Next, Response, Request } from "@decorators/express";
import { VerifyToken, GenerateToken, SendToken } from "../middleware/JWT";


@Controller('/account')
export class TokenController {

  @Post('/login', [GenerateToken, SendToken])
  login(@Request() req: Req, @Response() res: Res, @Next() next: any) {
    next();
  }

  @Post('/register', [GenerateToken, SendToken])
  register(@Request() req: Req,@Response() res: Res){
  }

  @Get('/google/redirect', [GenerateToken, SendToken])
  socialToken(@Request() req: Req, @Response() res: Res,@Next() next: NextFunction){

  }

}
