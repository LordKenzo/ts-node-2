import { Account } from "../models/Account";
import { Request as Req, Response as Res, NextFunction } from "express";
import { Controller, Post, Get, Next, Response, Request } from "@decorators/express";
import { checkEmailPassword } from "../middleware/checkRequest";
import passport from 'passport';

@Controller('/account')
export class AccountController {

  @Post('/login')
  async login(@Request() req: Req, @Response() res: Res, @Next() next: any) {
    try {
      let user = await Account.findAccount(req.body.email );
      if (user === null) throw "Utente non trovato";
      if(user.meta.provider === 'local' && user.password){
        let success = await Account.comparePassword(user.password, req.body.password);
        if (success === false) throw "Errore di login";
      }
      req.user = user;
      next();


    } catch (err) {
      console.debug(err);
      res.status(401).json({ message: "Credenziali non valide", errors: err });
    }
  }

  @Post('/register', [checkEmailPassword])
  async register(@Request() req: Req,@Response() res: Res){
    try{
      const data = {
        firstName: req.body.firstName || null,
        lastName: req.body.lastName || null,
        email: req.body.email,
        password: req.body.password,
        username: req.body.username || null,
        displayName: req.body.displayName || null,
        meta: {
          provider: 'local',
          providerIdentifierField: 'id',
          providerData: null
        }
      };
      const account = await Account.createAccount(data);
      res.status(201).json({"message":"Utente creato correttamente!","id": account._id});
    } catch(err){
      res.status(400).json({"message":"Impossibile creare l'utente", errors: err});
    }

  }

  // Social Routes
  @Get('/google')
  google(@Request() req: Req,@Response() res: Res,@Next() next: NextFunction){
    passport.authenticate('google', { session: false, scope: ['openid', 'profile', 'email'] })(req, res, next);
  }

  @Get('/google/redirect')
  googleRedirect(@Request() req: Req,@Response() res: Res, @Next() next: NextFunction){
    passport.authenticate('google', { session: false }, function(err, user){
      next();
    })(req, res);

  }
}
