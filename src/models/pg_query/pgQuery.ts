import ORM from 'sequelize';
import { ProfileModel } from '../Profile';

export class pgQuery{
  constructor(public table: ORM.Model<{}, {}>){
  }

  findAll(options: ORM.FindOptions<any> |undefined) : Promise<any | null> {
    let promise = new Promise<any | null>((resolve, reject) => {
      this.table.findAll(options)
      .then(res => {
        //ho una doppia resolve -.-'
        resolve(resolve(res))
      })
      .catch(err => reject(err));
    });
    return promise;
  }
}

export class Profile extends pgQuery{
  profile: any;
  constructor(table:any, dbConnection:any){
    super(table);
    this.profile = ProfileModel(dbConnection);
  }
}


