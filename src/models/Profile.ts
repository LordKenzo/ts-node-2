import ORM, { Sequelize } from "Sequelize";

function initProfile(sequelize: Sequelize){
  // Con sequelize.define definisco il modello
  return sequelize.define('Profiles', {
    account: ORM.STRING,
  });
}

export const ProfileModel = initProfile;