import mongoose from 'mongoose';
import * as bcrypt from "bcryptjs";
import { BaseModel } from './BaseModel';

export interface IAccount extends mongoose.Document{
  firstName?: string,
  lastName?: string,
  displayName?: string,
  username?: string;
  password?: string;
  email: string;
  meta: {
    provider: string;
    providerIdentifierField: string;
  };
}

let schema = new mongoose.Schema({
  firstName: String,
  lastName: String,
  displayName: String,
  username: String,
  email: {
    type: String,
    trim: true,
    minlength: 9,
    required: true
  },
  password: String,
  meta: {
    provider: String,
    providerIdentifierField: String,
  }
});

schema.pre("save", function(this: any, next) {
  if(this.password){
    bcrypt.hash(this.password, 10, (err, hash) => {
      this.password = hash;
      console.log("Save in Account", this);
      next();
    });
  } else {
    next()
  }

});

schema.pre("update", async function(this: IAccount, next) {
  if(this.password){
    await bcrypt.hash(this.password, 10, (err, hash) => {
      this.password = hash;
      next();
    });
  }

});

export let AccountSchema = mongoose.model<IAccount>('Account', schema, 'accounts');

export class AccountRepository extends BaseModel<IAccount> {
  constructor() {
    super(AccountSchema);
  }
}

export class Account {
  private _AccountDocument: IAccount;

  constructor(account:IAccount){
    this._AccountDocument = account;
  }

  get username(): string | undefined {
    return this._AccountDocument.username;
  }

  get password(): string | undefined{
    return this._AccountDocument.password;
  }

  get firstName(): string | undefined{
    return this._AccountDocument.firstName;
  }

  get lastName(): string | undefined{
    return this._AccountDocument.lastName;
  }

  get displayName(): string | undefined{
    return this._AccountDocument.displayName;
  }

  get meta(): {provider: string, providerIdentifierField: string} {
    return this._AccountDocument.meta;
  }

  static createAccount(data: any) : Promise<IAccount> {
    let promise = new Promise<IAccount>((resolve, reject) => {

      let repo = new AccountRepository();

      let account = <IAccount>{
        firstName: data.firstName,
        lastName: data.lastName,
        email: data.email,
        password: data.password,
        displayName: data.displayName,
        username: data.username,
        meta: {
          provider: data.meta.provider,
          providerIdentifierField: data.meta.providerIdentifierField
        }
      };
      console.debug('Account Class::createAccount');

      repo.create(account, (err: any, saved: IAccount) => {
        if (err) { reject(err); }
        else { resolve(saved); }
      });

    });

    return promise;

  }

  static findAccount(email: string) : Promise<IAccount | null> {
    let promise = new Promise<IAccount | null>((resolve, reject) => {
      console.debug('Account Class::findAccount');
      let repo = new AccountRepository();

      repo.find({ email : email }).sort({ createdAt: -1 }).limit(1).exec((err, res) => {
        if (err) {
          reject(err);
        }
        else {
          if (res && res.length) {
            resolve(<IAccount>res[0]);
          }
          else {
            resolve(null);
          }
        }
      });
    });

    return promise;
  }

  static comparePassword(password: string, candidatePassword: string): Promise<boolean> {
    console.debug('Account Class::comparePassword');
    return new Promise((resolve: any, reject: any) => {
        bcrypt.compare(candidatePassword, password, (err, success) => {
            if (err) return reject(err);
            return resolve(success);
        });
    });
  }

}
/*
schema.statics.findUserByEmail = function(email: string, callback: any) {
  console.debug('findUserByEmail', email)
  this.findOne({ email: email }, function(err: any, usr: IAccount) {
    if (err || !usr) {
      callback(err, null);
    } else {
      callback(false, usr);
    }
  });
};

schema.statics.loginByEmail = function(email: string, password: string, callback: any) {
  console.debug('loginByEmail', email, password)
  this.findOne({ email: email}, function(err: any, usr: IAccount) {
    if (err || !usr) {
      callback(err);
    } else {
      usr.comparePassword(password)
      .then(success => {
        if (success === false) {
          callback('Password errata');
        }
        else {
          callback(false, usr);
        }
      });
    }
  });
};
*/

//export const Account: IUserModel = mongoose.model<IUser, IUserModel>("Account", AccountSchema);

//export const Account2 = mongoose.model<IAccount>("Account", schema);

// DEPRECATO:
//export const Account = mongoose.model<IUser>("User", AccountSchema);