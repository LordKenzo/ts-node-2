export const config = {
    server: {
      API: '/v1',
      PORT: 8080,
      COOKIE_KEY: 'asdfsadfsadffsd',
      SECRET_JWT: 'dsfadfdsfasdfsadf',
      EXPIRES_JWT: 1000,
      ENV: 'development',
      DEV_DOMAIN: 'http://localhost:8080',
    },
    social: {
      google: {
        clientId:'xxxx-.apps.googleusercontent.com',
        clientSecret:'xxxxx',
        callbackUrl:'http://localhost:8080/v1/account/google/redirect'
      },
      facebook: {

      },
    },
    mongodb: {
      mongoURI: {
        dev: 'mongodb://user:psw@ds145438.mlab.com:45438/dbname',
        test: 'mongodb://user:psw@ds145438.mlab.com:45438/dbname',
        prod: 'mongodb://user:psw@ds145438.mlab.com:45438/dbname'
      }
    },
    postgreSql: {
      development: {
        url: 'postgres://user:psw@localhost:5432/dbname',
        dialect: 'postgres',
        operatorsAliases: false,
      },
      web: {
        url: 'postgres://user:psw@horton.elephantsql.com:5432/dbname',
        dialect: 'postgres',
        operatorsAliases: false,
      },
      production: {
        url: '',
        dialect: 'postgres',
        operatorsAliases: false,
      },
    },
  };
