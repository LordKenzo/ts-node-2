import Sequelize from "sequelize";

import {config} from '.';

const url = config.postgreSql.web.url || process.env.DATABSE_CONNECTION_URI || 'postgres://postgres:postgres@localhost:5432/postgres',
    dbConnection = new Sequelize(url, config.postgreSql.web);

export {dbConnection};