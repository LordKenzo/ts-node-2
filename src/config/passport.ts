import {Application, Request} from 'express';
import passport from "passport";
import {Account, IAccount, AccountRepository} from '../models/Account';
import { config } from '.';
import {Strategy as GoogleStrategy, VerifyFunction} from 'passport-google-oauth2';

export const initPassport = (app: Application) => {
  app.use(passport.initialize());
  app.use(passport.session());

  passport.serializeUser((user, done) => {
    done(undefined, user);
  });

  passport.deserializeUser((id: string, done) => {
    new AccountRepository().findById(id, (err, user: IAccount | null) => {
      if(!user){
        done(err);
      } else {
        done(null, user);
      }
    });

  });

  const passportGoogleConfig = {
    clientID: config.social.google.clientId,
    clientSecret: config.social.google.clientSecret,
    callbackURL: config.social.google.callbackUrl,
    passReqToCallback: true
  };

  if (passportGoogleConfig.clientID) {
    passport.use(new GoogleStrategy({
      clientID: config.social.google.clientId,
      clientSecret: config.social.google.clientSecret,
      callbackURL: config.social.google.callbackUrl,
      passReqToCallback: true
    },
    function(req : Request, accessToken, refreshToken, profile, done) {
      // Da togliere?
      const providerData = profile._json;
      providerData.accessToken = accessToken;
      providerData.refreshToken = refreshToken;
      console.log('Social Token', accessToken);
      const socialProfile = {
        firstName: profile.name.givenName,
        lastName: profile.name.familyName,
        displayName: profile.displayName,
        email: profile.emails[0].value,
        username: profile.username,
        meta: {
          provider: 'google',
          providerIdentifierField: 'id'
        }
      };

      new AccountRepository().findOne({email:socialProfile.email}, async function(err, existingUser) {
        if(!existingUser) {
          const account = await Account.createAccount(socialProfile);
          console.log('account', account)
          req.user = account;
          done(null, account);
        } else {
          console.log('EXU', existingUser)
          req.user = existingUser;
          done(null, existingUser);
        }
    });
    }
  ));
  }

}
