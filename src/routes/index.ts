import express, {Application} from 'express';
import initDb from '../config/mongodb';
import { attachControllers } from '@decorators/express';
import { AccountController } from '../controller/account';
import { TokenController } from '../controller/token';

const router: Application = express();

initDb((db:Promise<void>)=> {
  attachControllers(router, [AccountController, TokenController]);
});

export default router;
