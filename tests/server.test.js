import request from 'request';
import { profile } from '../src/models/Model';
import { Profile } from '../src/models/pg_query/pgQuery';
import { dbConnection } from "../src/config/postgresql";
import { ProfileModel } from '../src/models/Profile';

describe('Server is UP', function(){
  it('should respond to a GET request', function(done){
    request.get({
      url: `http://localhost:8080`,
      json: true
    }, function(err, res, body){
      expect(res.statusCode).toBe(200);
      expect(body).toEqual({ 'Page' :'Home Page' });
      done();
    });
  });
  it('should respond an About Page', function(done){
    request.get({
      headers: {
        'Authorization': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1MjA1MzY1NTIsImV4cCI6MTUyMDUzNzU1Mn0.Je9R4V5yrdsW_8SZF-E-raeoyFVrz1wvMXkk34PJCAA'
      },
      url: `http://localhost:8080/v1/about`,
      json: true
    }, function(err, res, body){
      expect(res.statusCode).toBe(200);
      expect(body).toEqual({ 'Page' :'About Page' });
      done();
    });
  });
});

describe('Test PostgreSQL', function(){
  beforeAll( function(){
    const profileModel = ProfileModel(dbConnection);
  });

  it("not should return an empty array", async function(){
    const profileModel = ProfileModel(dbConnection);
    let profile = new Profile(profileModel, dbConnection);
    await expect( profile.findAll()).resolves.not.toEqual([]);
  });
})