-- Database: "ts-node-meetup"


-- DROP TABLE "Profiles";

CREATE TABLE "Profiles"
(
  id serial NOT NULL,
  account character varying(255) NOT NULL,
  CONSTRAINT "Profile_pkey" PRIMARY KEY (id),
  "createdAt" timestamp with time zone NOT NULL,
  "updatedAt" timestamp with time zone NOT NULL
);

/*
INSERT INTO "public"."Profiles" (id, account, "createdAt", "updatedAt") VALUES (1, 'asdaf', '2005-04-03 12:00-06', '2005-04-03 12:00-06')
*/